<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PersonaController extends Controller
{
    public function saludoPersonalizado($nombre, $edad)
    {
        return view("Persona.saludopersonalizado", compact('nombre', 'edad'));
    }

    public function create()
    {
        return view("Persona.create");
    }

    public function store(Request $request)
    {
        $id = $request->input('id');
        $nombre = $request->input('nombre');
        $edad = $request->input('edad');

        // Guardar datos

        return view("Persona.saludopersonalizado", compact('nombre', 'edad'));

    }
}
