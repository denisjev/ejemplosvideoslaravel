@extends('persona.layoutpersona')

@section('contenido')
    <form action="{{ route('personas.store') }}" method="post">
        @csrf
        Id: <input type="number" class="form-control" name="id" />
        Nombre: <input type="text" class="form-control" name="nombre" />
        Edad: <input type="number" class="form-control" name="edad" />
        <input class="btn btn-danger" type="submit" value="Guardar" />
    </form>
@endsection
