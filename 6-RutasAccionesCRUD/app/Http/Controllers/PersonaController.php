<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Persona;

class PersonaController extends Controller
{
    public function saludoPersonalizado($nombre, $edad)
    {
        return view("Persona.saludopersonalizado", compact('nombre', 'edad'));
    }

    public function index()
    {
        /** Obtenemos la lista de elementos */
        $listaPersonas = Persona::listAll();
        $message = "";

        return view('Persona.index', compact("listaPersonas", "message"));
    }


    public function create()
    {
        return view("Persona.create");
    }

    public function store(Request $request)
    {
        $id = $request->input('id');
        $nombre = $request->input('nombre');
        $edad = $request->input('edad');

        /** Creamos el nuevo objeto */
        $persona = new Persona($id, $nombre, $edad);

        /** Simulamos que almacenamos los datos */
        $listaPersonas = Persona::listAll();
        array_push($listaPersonas, $persona);

        $message = "Dato agregado correctamente";

        /** Una vez guardado el nuevo dato volvemos al Index */
        return view('Persona.index', compact("listaPersonas", "message"));

    }

    public function show($id)
    {
        /** Se busca al elemento que deseeamos encontrar y una vez encontrado se pasa
         * a la vista */
        $persona = new Persona(1, "Denis Espinoza", 34);
        return view("Persona.show", compact("persona"));
    }

    public function edit($id)
    {
        /** Con base al id recibido por parámetro, buscamos el dato correspondiente para editar
         * en este caso editamos el primero */
        $persona = Persona::listAll()[0];

        /** Mostramos la interfaz para editar el dato */
        return view("Persona.edit", compact("persona"));
    }

    public function update(Request $request, $id)
    {
        $listaPersonas = Persona::listAll();
        /** Buscamos el elemento indicado por la variable id dentro de listaPersonas
         * y de la variable request tomamos los datos que se deben actualizar  */

        $message = "Dato actualizado correctamente";

        /** Una vez guardado el nuevo dato volvemos al Index */
        return view('Persona.index', compact("listaPersonas", "message"));
    }

    public function destroy($id)
    {
        /** Con base al id recibido por parámetro, eliminamos el dato correspondiente
         * en este caso lo simulamos eliminando el último */
        $listaPersonas = Persona::listAll();
        array_pop($listaPersonas);
        $message = "Dato borrado";

        /** Una vez hecho el borrado volvemos al Index */
        return view('Persona.index', compact("listaPersonas", "message"));
    }
}
