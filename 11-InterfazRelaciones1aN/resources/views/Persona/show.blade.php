@extends('persona.layoutpersona')

@section('contenido')
<div class="card" style="width: 18rem;">
    <div class="card-body">
        <h5 class="card-title">{{ $persona->nombre }}</h5>
        <h6 class="card-subtitle mb-2 text-muted">Edad: {{ $persona->edad }}</h6>
        <p class="card-text">Id: {{ $persona->id }}</p>

        <a href="{{ route('personas.edit', ['id' => $persona->id]) }}"
            class="btn btn-warning" role="button">Editar</a>

        <form action="{{ route('personas.destroy', ['id' => $persona->id]) }}" method="post">
            @method('DELETE')
            @csrf
            <input class="btn btn-danger" type="submit" value="Delete" />
        </form>

        <b>Correos electrónicos</b><br>

        <b><i>Agregar correo</i></b>
        <form action="{{ route('correos.store') }}" method="post">
            @csrf
            <input type="hidden" name="id_persona" value="{{ $persona->id }}" />
            <input class="form-control" type="text" name="direccion" value="" />
            <input class="btn btn-success" type="submit" value="Agregar" />
        </form>
        <br>
        <b><i>Listado de correos</i></b>
        <table>
        @foreach ($persona->correos as $correo)
            <tr>
                <td>{{ $correo->direccion }}</td>
                <td>
                    <form action="{{ route('correos.destroy', ['id_correo' => $correo->id, 'id_persona' => $correo->id_persona]) }}" method="post">
                        @method('DELETE')
                        @csrf
                        <input class="btn btn-danger" type="hidden" name="id_persona" value="{{ $correo->id_persona }}" />
                        <input class="btn btn-danger" type="submit" value="Delete" />
                    </form>
               </td>
            </tr>
        @endforeach
        </table>
    </div>
  </div>
@endsection
