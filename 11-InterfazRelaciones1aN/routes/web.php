<?php

use App\Http\Controllers\PersonaController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/saludo/{nombre}/{edad}', 'PersonaController@saludoPersonalizado');

/** Listar todos los elementos  */
Route::get("/personas", "PersonaController@index")->name("personas.index");

/** Agregar un nuevo elemento */
Route::get('/personas/create', 'PersonaController@create')->name("personas.create");
Route::post('/personas', 'PersonaController@store')->name("personas.store");

/** Muestra los datos de una persona */
Route::get("/personas/{id}", "PersonaController@show")->name("personas.show");

/** Muestra el formualario para editar los datos de una persona */
Route::get("/personas/{id}/edit", "PersonaController@edit")->name("personas.edit");
/** Guarda los datos del formualario de editar persona */
Route::put("/personas/{id}", "PersonaController@update")->name("personas.update");

/** Elimina los datos de una persona */
Route::delete("/personas/{id}", "PersonaController@destroy")->name("personas.destroy");



Route::post('/correos', 'CorreoController@store')->name("correos.store");
Route::delete("/correos/{id_correo}/{id_persona}", "CorreoController@destroy")->name("correos.destroy");
