<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Correo extends Model
{
    protected $table = "correo";

    public function persona()
    {
        return $this->belongsTo('App\Models\Persona', 'id_persona');
    }
}
