<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
    protected $fillable = [ 'nombre', 'edad' ];

    public function correos()
    {
        return $this->hasMany('App\Models\Correo', 'id_persona');
    }
}
