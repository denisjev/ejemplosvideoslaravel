<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Correo;
use App\Models\Persona;

class CorreoController extends Controller
{
    public function store(Request $request)
    {
        $direccion = $request->input('direccion');
        $id_persona = $request->input('id_persona');

        /** Creamos el nuevo objeto */
        $correo = new Correo();
        $correo->direccion = $direccion;
        $correo->id_persona = $id_persona;

        $affected = $correo->save();
        $persona = Persona::find($id_persona);

        /** Mostramos la interfaz para editar el dato */
        return view("Persona.show", compact("persona"));
    }

    public function destroy($id_correo, $id_persona)
    {
        $affected = Correo::find($id_correo)->delete();
        $persona = Persona::find($id_persona);

        /** Mostramos la interfaz para editar el dato */
        return view("Persona.show", compact("persona"));
    }
}
