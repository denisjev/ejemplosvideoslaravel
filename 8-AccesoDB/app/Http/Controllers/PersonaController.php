<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Persona;
use Illuminate\Support\Facades\DB;


class PersonaController extends Controller
{
    public function saludoPersonalizado($nombre, $edad)
    {
        return view("Persona.saludopersonalizado", compact('nombre', 'edad'));
    }

    public function index()
    {
        /** Obtenemos la lista de elementos */
        $listaPersonas = DB::table('personas')->get();
        $message = "";

        return view('Persona.index', compact("listaPersonas", "message"));
    }


    public function create()
    {
        return view("Persona.create");
    }


    public function store(Request $request)
    {
        $id = $request->input('id');
        $nombre = $request->input('nombre');
        $edad = $request->input('edad');

        /** Creamos el nuevo objeto */
        $persona = new Persona($id, $nombre, $edad);

        $affected= DB::table('personas')->insert((array)$persona);

        if($affected > 0)
            $message = "Dato agregado";
        else
            $message = "Dato no agregado";

        $listaPersonas = DB::table('personas')->get();

        /** Una vez guardado el nuevo dato volvemos al Index */
        return view('Persona.index', compact("listaPersonas", "message"));

    }

    public function show($id)
    {
        $persona = DB::table('personas')->where('id', $id)->get()[0];

        return view("Persona.show", compact("persona"));
    }

    public function edit($id)
    {
        $persona = DB::table('personas')->where('id', $id)->get()[0];

        /** Mostramos la interfaz para editar el dato */
        return view("Persona.edit", compact("persona"));
    }

    public function update(Request $request, $id)
    {
        $nombre = $request->input('nombre');
        $edad = $request->input('edad');

        $affected = DB::table('personas')->where('id', $id)->update(['nombre' => $nombre, 'edad' => $edad]);

        if($affected > 0)
            $message = "Dato actualizado";
        else
            $message = "Dato no actualizado";

        $listaPersonas = DB::table('personas')->get();


        /** Una vez guardado el nuevo dato volvemos al Index */
        return view('Persona.index', compact("listaPersonas", "message"));
    }

    public function destroy($id)
    {
        $affected = DB::table('personas')->where('id', $id)->delete();

        if($affected > 0)
            $message = "Dato borrado";
        else
            $message = "Dato no borrado";

        $listaPersonas = DB::table('personas')->get();

        /** Una vez hecho el borrado volvemos al Index */
        return view('Persona.index', compact("listaPersonas", "message"));
    }

}
