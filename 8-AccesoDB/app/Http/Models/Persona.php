<?php

namespace App\Http\Models;

class Persona
{
    public $id;
    public $nombre;
    public $edad;

    public function __construct($i=0, $n = "", $e = 0)
    {
        $this->id = $i;
        $this->nombre = $n;
        $this->edad = $e;
    }

    public static function listAll()
    {
        $listaPersonas = array();
        array_push($listaPersonas, new Persona(1, "Denis Espinoza", 34));
        array_push($listaPersonas, new Persona(2, "Maria Vargas", 25));
        array_push($listaPersonas, new Persona(3, "Juan Mendoza", 30));

        return $listaPersonas;
    }
}
