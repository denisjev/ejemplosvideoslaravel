<html>
<head>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>

<body>
    <h1>Página de personas</h1>
    <hr>
    <div class="row">
      <div class="col-md-3"></div>

      <div class="col-md-6">
        @yield('contenido')
      </div>

      <div class="col-md-3"></div>
    </div>
    <hr>
</body>
</html>
