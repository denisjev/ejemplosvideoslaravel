@extends('persona.layoutpersona')

@section('contenido')
    <form action="{{ route('personas.update', ['id' => $persona->id]) }}" method="post">
        @method('PUT')
        @csrf
        Id: <input type="number" class="form-control" name="id" value="{{ $persona->id }}" readonly />
        Nombre: <input type="text" class="form-control" name="nombre" value="{{ $persona->nombre }}" />
        Edad: <input type="number" class="form-control" name="edad" value="{{ $persona->edad }}" />
        <input class="btn btn-danger" type="submit" value="Guardar" />

    </form>
@endsection
