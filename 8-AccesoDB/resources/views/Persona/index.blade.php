@extends('persona.layoutpersona')

@section('contenido')

<div class="alert alert-primary" role="alert">
  {{ $message }}
 </div>

<a href="{{ route('personas.create') }}" class="btn btn-success" role="button">Agregar</a>
<br>

<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">Nombre</th>
      <th scope="col">Ver</th>
    </tr>
  </thead>
  <tbody>


@foreach ($listaPersonas as $persona)
    <tr>
      <td>{{ $persona->nombre }}</td>
      <td>
        <a href="{{ route('personas.show', ['id' => $persona->id]) }}"
            class="btn btn-primary" role="button">Detalles</a>
        </td>
    </tr>
@endforeach

</tbody>
</table>

@endsection
